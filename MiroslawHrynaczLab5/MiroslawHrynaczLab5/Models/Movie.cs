﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MiroslawHrynaczLab5.Models
{
    public class Movie
    {
        public int Id { set; get; }

        [Display(Name = "Nazwa")]
        public string Name { set; get; }

        [DataType(DataType.Date)]
        [Display(Name = "Data Wydania")]
        public DateTime ReleaseTime { set; get; }
    }
}