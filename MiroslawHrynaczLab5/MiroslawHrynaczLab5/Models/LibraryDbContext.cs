﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MiroslawHrynaczLab5.Models
{
    public class LibraryDbContext : DbContext
    {
        public LibraryDbContext()
            : base("DefaultConnection"){}

        public virtual DbSet<Movie> Movie { set; get; }
    }
}