﻿using System;
using System.Linq;
using System.Web.Mvc;
using MiroslawHrynaczLab5.Models;

namespace MiroslawHrynaczLab5.Controllers
{
    public class MoviesController : Controller
    {
        public LibraryDbContext context = new LibraryDbContext();
        //
        // GET: /Movie/

        public ActionResult Index()
        {
            return View(new Movie() {ReleaseTime = DateTime.Now});
        }

        public void InitData()
        {
            context.Movie.Add(new Movie() {Name = "Dom", ReleaseTime = new DateTime(2015, 5, 27)});
            context.Movie.Add(new Movie() {Name = "Dom2", ReleaseTime = new DateTime(2014, 5, 7)});
            context.Movie.Add(new Movie() {Name = "Do4", ReleaseTime = new DateTime(2013, 5, 2)});
            context.Movie.Add(new Movie() {Name = "Doom", ReleaseTime = new DateTime(2015, 3, 17)});
            context.Movie.Add(new Movie() {Name = "Damy", ReleaseTime = new DateTime(2015, 2, 22)});
            context.Movie.Add(new Movie() {Name = "Domy", ReleaseTime = new DateTime(2015, 1, 4)});
            context.SaveChanges();
        }

        public ActionResult DeleteById(int id)
        {
            var movieToDelete = (from movie in context.Movie where movie.Id == id select movie).FirstOrDefault();
            context.Movie.Remove(movieToDelete);
            context.SaveChanges();
            return RedirectToAction("ShowAll");
        }

        public ActionResult UpdateById(int id)
        {
            Movie movieToUpdate = context.Movie.Find(id);
            return View(movieToUpdate);
        }

        public ActionResult ShowAll()
        {
            var query = from movie in context.Movie select movie;
            return View(query.AsEnumerable());
        }

        [HttpPost]
        public ActionResult AddNewMovie(Movie newMovie)
        {
            context.Movie.Add(newMovie);
            context.SaveChanges();
            return RedirectToAction("ShowAll");
        }

        [HttpPost]
        public ActionResult save(Movie updatedMovie)
        {
            var movieToEdit = context.Movie.Find(updatedMovie.Id);
            movieToEdit.Name = updatedMovie.Name;
            movieToEdit.ReleaseTime = updatedMovie.ReleaseTime;
            context.SaveChanges();
            return RedirectToAction("ShowAll");
        }
    }
}
