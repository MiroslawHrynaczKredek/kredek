﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AForge.Imaging;
using AForge.Imaging.Filters;

namespace HoughViewer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            
            var window = new OpenFileDialog();
            window.ShowDialog();
            var image = new Bitmap(window.FileName);

            image = GrayscaleBT709.CommonAlgorithms.BT709.Apply(image);
            //image = new ResizeBicubic(image.Width, image.Height / 2).Apply(image);

            var hlt = new HoughLineTransformation();
            hlt.ProcessImage(image);

            image = hlt.ToBitmap();
            new SISThreshold().ApplyInPlace(image);


            var se = new short[,] { { 0, 1, 0 }, { 0, 1, 0 }, { 0, 1, 0 } };
            new Dilatation(se).ApplyInPlace(image);
            new Invert().ApplyInPlace(image);

            image = new ConnectedComponentsLabeling().Apply(image);

            pictureBox1.Image = image;
            pictureBox1.Update();
        }
    }
}
