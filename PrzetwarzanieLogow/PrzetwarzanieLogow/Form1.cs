﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace PrzetwarzanieLogow
{
    public partial class Form1 : Form
    {
        private string fileName = "";

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            richTextBox1.Text = "Statystyki (...)";

            var window = new OpenFileDialog();
            if (window.ShowDialog() == DialogResult.OK)
            {
                fileName = window.FileName;
            }

            var reader = new StreamReader(fileName);
            var lines = new List<string>();

            var packages = new List<List<string>>(); 

            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                lines.Add(line);

                if (line.StartsWith("DONE"))
                {
                    packages.Add(new List<string>(lines));
                    lines = new List<string>();
                }
            }
            reader.Close();
            richTextBox1.Text = @"Szczegóły (...)";

            var logs = new AllLog(packages);
            var processedLogs = logs.getLogs();
            listBox1.Items.AddRange(processedLogs.ToArray());
            
            richTextBox1.Text = logs.getStatistic();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            richTextBox1.Text = (listBox1.Items[listBox1.SelectedIndex] as OneLog).getContent();
        }
    }

    class AllLog
    {
        private List<OneLog> logs = new List<OneLog>(); 

        public AllLog(List<List<string>> p_packages)
        {
            foreach (var package in p_packages)
            {
                logs.Add(new OneLog(package));
            }
        }

        public List<OneLog> getLogs()
        {
            return logs;
        }

        public string getStatistic()
        {
            var strA = "";
            var strB = "";
            var strC = "";
            var strD = "";

            for (int index = 3; index < logs.Count; index+=4)
            {
                var item = logs[index];
                strA += item.getWrongLocalization() + "^";
                strB += item.getWrongAngleDetection() + "^";
                strC += item.getWrongRecognition() + "^";
                strD += item.getContent() + "^";
            }
            return strA + "\n" + strB + "\n" + strC + "\n" + strD + "\n";
        }
    }

    class OneLog
    {
        List<string> header = new List<string>();
        List<string> body = new List<string>();

        List<List<string>> pairs = new List<List<string>>();
        private int wrongLocalization = 0;
        private int wrongRecognition = 0;
        private int wrongAngleDetection = 0;

        public OneLog(List<string> p_package)
        {
            var start = 0;
            var end = 0;

            while (!p_package[++start].StartsWith("Lok")){}
            while (!p_package[++end].StartsWith("Blur")){}

            header.AddRange(p_package.GetRange(start, end - start + 1));
            p_package.RemoveRange(0, end + 1);

            body = new List<string>(p_package);

            splitOnPairs();
            verifyPairs();
        }

        private void splitOnPairs()
        {
            var pair = new List<string>();
            var start = 0;
            var end = 0;
            for (int index = 0; index < body.Count; index++)
            {
                if (body[index] == "")
                {
                    end = index - 1;
                    pair = body.GetRange(start, end - start + 1);
                    pairs.Add(pair);
                    start = index + 1;
                }
            }
        }

        private void verifyPairs()
        {
            foreach (var pair in pairs)
            {
                if (pair.Count != 2)
                {
                    ++wrongLocalization;
                }
                if (pair[1].StartsWith("NOK"))
                {
                    ++wrongRecognition;
                }
                if (pair[1].Count(x => x == '#') > 4 || pair[1].Contains("brak"))
                {
                    ++wrongAngleDetection;
                }
            }
        }

        public override string ToString()
        {
            return "wynik";
        }

        public string getContent()
        {
            var name = "";
            foreach (var column in header)
            {
                name += column + "\n";
            }
            name += String.Format("Błędnie :\n");
            name += String.Format("Lokalizacja {0}/{1}\n", wrongLocalization, pairs.Count);
            name += String.Format("Detekcja kąta {0}/{1}\n", wrongAngleDetection, pairs.Count);
            name += String.Format("Rozpoznanie {0}/{1}\n", wrongRecognition, pairs.Count);

            return name;
        }


        public int getWrongLocalization()
        {
            return wrongLocalization;
        }

        public int getWrongRecognition()
        {
            return wrongRecognition;
        }

        public int getWrongAngleDetection()
        {
            return wrongAngleDetection;
        }

        public int getNrOfFrames()
        {
            return pairs.Count;
        }
    }
}
