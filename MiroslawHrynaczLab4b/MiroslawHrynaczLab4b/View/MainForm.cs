﻿using System;
using System.Linq;
using System.Windows.Forms;
using MiroslawHrynaczLab4b.Model;
using Customer = MiroslawHrynaczLab4b.Model.Customer;

namespace MiroslawHrynaczLab4b.View
{
    public partial class MainForm : Form
    {
        DataClassesNorthwndDataContext dataContext = new DataClassesNorthwndDataContext();

        public MainForm()
        {
            InitializeComponent();
        }

        private void buttonGet_Click(object sender, EventArgs e)
        {
            dataGridViewContent.DataSource = Customer.GetAll(dataContext);
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            var client = (from customer in dataContext.Customers where customer.Country.StartsWith("Spain") select customer);
            foreach (var customer in client)
            {
                customer.City = "Wrocław";
            }
            dataContext.SubmitChanges();
            buttonGet_Click(null, null);
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            Customer customer = new Customer
            {
                CustomerID = "Jax33",
                ContactName = "Mirek",
                Address = "Legn",
                City = "Wroc",
                CompanyName = "pol",
                Country = "Pol",
                ContactTitle = "Mr",
                Fax = "none",
                Phone = "50",
                PostalCode = "58-2"
            };
            try
            {
                dataContext.Customers.InsertOnSubmit(customer);
                dataContext.SubmitChanges();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {
            var client = (from customer in dataContext.Customers select customer).First(x => x.ContactName.StartsWith("Mir"));
            dataContext.Customers.DeleteOnSubmit(client);
            dataContext.SubmitChanges();
            buttonGet_Click(null, null);
        }

        private void buttonOrder_Click(object sender, EventArgs e)
        {
            var query = from customer in dataContext.Customers
                join order in dataContext.Orders on customer.CustomerID equals order.CustomerID select new { Klient = customer.ContactName , DataZamowienia = order.OrderDate };
            dataGridViewContent.DataSource = query;

            Customer.GetAll(dataContext);
            Customer.GetByName("Janek", dataContext);
        }
    }
}
