﻿using System.Linq;

namespace MiroslawHrynaczLab4b.Model
{
    public partial class Customer
    {
        public static IQueryable<Customer> GetAll(DataClassesNorthwndDataContext dataContext)
        {
            var query = from customer in dataContext.Customers select customer;
            return query;
        }

        public static IQueryable<Customer> GetByName(string name, DataClassesNorthwndDataContext dataContext)
        {
            var query = from customer in dataContext.Customers where customer.ContactName.StartsWith(name) select customer;
            return query;
        }
    }
}