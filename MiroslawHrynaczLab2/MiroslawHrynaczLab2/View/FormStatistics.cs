﻿using System.Windows.Forms;
using MiroslawHrynaczLab2.Controller;

namespace MiroslawHrynaczLab2.View
{
    public partial class FormStatistics : Form
    {
        #region Private Fields
        private readonly StatisticWriter statistics;
        #endregion

        #region Constructor
        public FormStatistics(DataGridView dataGridViewArrivals)
        {
            InitializeComponent();
            statistics = new StatisticWriter(dataGridViewArrivals, richTextBoxStatistics);
        }
        #endregion

        #region Events
        private void FormStatistics_FormClosing(object sender, FormClosingEventArgs e)
        {
            statistics.StopWriting();
        }
        #endregion

        public void Start()
        {
            statistics.StartWriting();
        }
    }
}
