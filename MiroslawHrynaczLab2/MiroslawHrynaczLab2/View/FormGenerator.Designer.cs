﻿namespace MiroslawHrynaczLab2.View
{
    partial class FormGenerator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainerGenerator = new System.Windows.Forms.SplitContainer();
            this.labelSpeed = new System.Windows.Forms.Label();
            this.trackBarSpeed = new System.Windows.Forms.TrackBar();
            this.buttonClose = new System.Windows.Forms.Button();
            this.buttonStopGenerating = new System.Windows.Forms.Button();
            this.buttonStartGenerating = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerGenerator)).BeginInit();
            this.splitContainerGenerator.Panel1.SuspendLayout();
            this.splitContainerGenerator.Panel2.SuspendLayout();
            this.splitContainerGenerator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarSpeed)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainerGenerator
            // 
            this.splitContainerGenerator.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerGenerator.Location = new System.Drawing.Point(0, 0);
            this.splitContainerGenerator.Name = "splitContainerGenerator";
            // 
            // splitContainerGenerator.Panel1
            // 
            this.splitContainerGenerator.Panel1.Controls.Add(this.labelSpeed);
            this.splitContainerGenerator.Panel1.Controls.Add(this.trackBarSpeed);
            // 
            // splitContainerGenerator.Panel2
            // 
            this.splitContainerGenerator.Panel2.Controls.Add(this.buttonClose);
            this.splitContainerGenerator.Panel2.Controls.Add(this.buttonStopGenerating);
            this.splitContainerGenerator.Panel2.Controls.Add(this.buttonStartGenerating);
            this.splitContainerGenerator.Size = new System.Drawing.Size(376, 262);
            this.splitContainerGenerator.SplitterDistance = 168;
            this.splitContainerGenerator.TabIndex = 0;
            // 
            // labelSpeed
            // 
            this.labelSpeed.AutoSize = true;
            this.labelSpeed.Location = new System.Drawing.Point(61, 87);
            this.labelSpeed.Name = "labelSpeed";
            this.labelSpeed.Size = new System.Drawing.Size(52, 13);
            this.labelSpeed.TabIndex = 1;
            this.labelSpeed.Text = "Prędkość";
            // 
            // trackBarSpeed
            // 
            this.trackBarSpeed.Location = new System.Drawing.Point(3, 106);
            this.trackBarSpeed.Maximum = 15;
            this.trackBarSpeed.Minimum = 1;
            this.trackBarSpeed.Name = "trackBarSpeed";
            this.trackBarSpeed.Size = new System.Drawing.Size(162, 45);
            this.trackBarSpeed.TabIndex = 0;
            this.trackBarSpeed.Value = 1;
            // 
            // buttonClose
            // 
            this.buttonClose.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonClose.Location = new System.Drawing.Point(0, 174);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(204, 87);
            this.buttonClose.TabIndex = 2;
            this.buttonClose.Text = "WYJŚCIE";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // buttonStopGenerating
            // 
            this.buttonStopGenerating.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonStopGenerating.Location = new System.Drawing.Point(0, 87);
            this.buttonStopGenerating.Name = "buttonStopGenerating";
            this.buttonStopGenerating.Size = new System.Drawing.Size(204, 87);
            this.buttonStopGenerating.TabIndex = 1;
            this.buttonStopGenerating.Text = "STOP";
            this.buttonStopGenerating.UseVisualStyleBackColor = true;
            this.buttonStopGenerating.Click += new System.EventHandler(this.buttonStopGenerating_Click);
            // 
            // buttonStartGenerating
            // 
            this.buttonStartGenerating.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonStartGenerating.Location = new System.Drawing.Point(0, 0);
            this.buttonStartGenerating.Name = "buttonStartGenerating";
            this.buttonStartGenerating.Size = new System.Drawing.Size(204, 87);
            this.buttonStartGenerating.TabIndex = 0;
            this.buttonStartGenerating.Text = "START";
            this.buttonStartGenerating.UseVisualStyleBackColor = true;
            this.buttonStartGenerating.Click += new System.EventHandler(this.buttonStartGenerating_Click);
            // 
            // FormGenerator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(376, 262);
            this.Controls.Add(this.splitContainerGenerator);
            this.Name = "FormGenerator";
            this.Text = "Generator";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormGenerator_FormClosing);
            this.splitContainerGenerator.Panel1.ResumeLayout(false);
            this.splitContainerGenerator.Panel1.PerformLayout();
            this.splitContainerGenerator.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerGenerator)).EndInit();
            this.splitContainerGenerator.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.trackBarSpeed)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainerGenerator;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Button buttonStopGenerating;
        private System.Windows.Forms.Button buttonStartGenerating;
        private System.Windows.Forms.Label labelSpeed;
        private System.Windows.Forms.TrackBar trackBarSpeed;
    }
}