﻿using System;
using System.Windows.Forms;
using MiroslawHrynaczLab2.Controller;

namespace MiroslawHrynaczLab2.View
{
    public partial class FormMain : Form
    {
        #region Private fields
        private FormGenerator formGenerator;
        private FormStatistics formStatistics;
        #endregion

        #region Constructor
        public FormMain()
        {
            InitializeComponent();
            formGenerator = new FormGenerator(dataGridViewArrivals);
            formGenerator.FormClosed += formGenerator_FormClosed;
        }
        #endregion

        #region Events
        private void buttonNew_Click(object sender, EventArgs e)
        {
            dataGridViewArrivals.Rows.Add(new object[]{textBoxRegisterNumber.Text, textBoxSupply.Text, textBoxAmount.Text});
        }

        private void buttonSaveToFile_Click(object sender, EventArgs e)
        {
            FormMainController.SaveToFile(dataGridViewArrivals);
        }

        private void buttonReadFromFile_Click(object sender, EventArgs e)
        {
            formGenerator.StopGenerating();
            FormMainController.LoadFromFile(dataGridViewArrivals);
        }

        private void textBoxFilter_TextChanged(object sender, EventArgs e)
        {
            formGenerator.StopGenerating();
            FormMainController.FilterRows(dataGridViewArrivals, textBoxFilter.Text);
        }

        private void buttonGenerateArrivals_Click(object sender, EventArgs e)
        {
            if (!formGenerator.Visible)
            {
                formGenerator.Show();
            }
        }

        void formGenerator_FormClosed(object sender, FormClosedEventArgs e)
        {
            formGenerator = new FormGenerator(dataGridViewArrivals);
            formGenerator.FormClosed += formGenerator_FormClosed;
        }

        private void buttonStatistics_Click(object sender, EventArgs e)
        {
            if (formStatistics == null)
            {
                formStatistics = new FormStatistics(dataGridViewArrivals);
                formStatistics.FormClosed += formStatistics_FormClosed;             
            }
            if (!formStatistics.Visible)
            {
                formStatistics.Show();
                formStatistics.Start();
            }
        }

        void formStatistics_FormClosed(object sender, FormClosedEventArgs e)
        {
            formStatistics = new FormStatistics(dataGridViewArrivals);
            formStatistics.FormClosed += formStatistics_FormClosed;
        }
        #endregion
    }
}
