﻿using System;
using System.Drawing;
using System.Windows.Forms;
using MiroslawHrynaczLab2.Controller;

namespace MiroslawHrynaczLab2.View
{
    public partial class FormGenerator : Form
    {
        #region Private Fields
        private DataGridView dataGridView;
        private readonly Generator generator;
        private int isRunning = 0;
        #endregion

        #region Constructor
        public FormGenerator(DataGridView dataGridViewArrivals)
        {
            InitializeComponent();
            dataGridView = dataGridViewArrivals;
            generator = new Generator(dataGridView, trackBarSpeed);

            buttonStopGenerating.BackColor = Color.DarkGray;
            buttonStartGenerating.BackColor = Color.DarkGray;
            buttonClose.BackColor = Color.DarkGray;
        }
        #endregion

        #region Events
        private void buttonStartGenerating_Click(object sender, EventArgs e)
        {
            buttonStopGenerating.BackColor = Color.DarkGray;
            buttonStartGenerating.BackColor = Color.LawnGreen;
            generator.StartGenerating();
        }

        private void buttonStopGenerating_Click(object sender, EventArgs e)
        {
            buttonStartGenerating.BackColor = Color.DarkGray;
            buttonStopGenerating.BackColor = Color.Red;
            generator.StopGenerating();
        }

        private void FormGenerator_FormClosing(object sender, FormClosingEventArgs e)
        {
            generator.StopGenerating();
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            generator.StopGenerating();
            Close();
        }
        #endregion

        public void StopGenerating()
        {
            generator.StopGenerating();
        }
    }
}
