﻿namespace MiroslawHrynaczLab2.View
{
    partial class FormStatistics
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.richTextBoxStatistics = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // richTextBoxStatistics
            // 
            this.richTextBoxStatistics.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBoxStatistics.Location = new System.Drawing.Point(0, 0);
            this.richTextBoxStatistics.Name = "richTextBoxStatistics";
            this.richTextBoxStatistics.Size = new System.Drawing.Size(284, 262);
            this.richTextBoxStatistics.TabIndex = 0;
            this.richTextBoxStatistics.Text = "";
            // 
            // FormStatistics
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.richTextBoxStatistics);
            this.Name = "FormStatistics";
            this.Text = "Statystyki";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormStatistics_FormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBoxStatistics;

    }
}