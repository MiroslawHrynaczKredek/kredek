﻿namespace MiroslawHrynaczLab2.View
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonNew = new System.Windows.Forms.Button();
            this.textBoxRegisterNumber = new System.Windows.Forms.TextBox();
            this.textBoxSupply = new System.Windows.Forms.TextBox();
            this.textBoxAmount = new System.Windows.Forms.TextBox();
            this.labelRegisterNumber = new System.Windows.Forms.Label();
            this.labelSupply = new System.Windows.Forms.Label();
            this.labelAmount = new System.Windows.Forms.Label();
            this.dataGridViewArrivals = new System.Windows.Forms.DataGridView();
            this.ColumnRegisterNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnSupply = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonSaveToFile = new System.Windows.Forms.Button();
            this.buttonReadFromFile = new System.Windows.Forms.Button();
            this.groupBoxOptions = new System.Windows.Forms.GroupBox();
            this.textBoxFilter = new System.Windows.Forms.TextBox();
            this.labelFilter = new System.Windows.Forms.Label();
            this.buttonGenerateArrivals = new System.Windows.Forms.Button();
            this.buttonStatistics = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewArrivals)).BeginInit();
            this.groupBoxOptions.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonNew
            // 
            this.buttonNew.Location = new System.Drawing.Point(11, 8);
            this.buttonNew.Name = "buttonNew";
            this.buttonNew.Size = new System.Drawing.Size(132, 39);
            this.buttonNew.TabIndex = 0;
            this.buttonNew.Text = "Nowy przyjazd";
            this.buttonNew.UseVisualStyleBackColor = true;
            this.buttonNew.Click += new System.EventHandler(this.buttonNew_Click);
            // 
            // textBoxRegisterNumber
            // 
            this.textBoxRegisterNumber.Location = new System.Drawing.Point(148, 24);
            this.textBoxRegisterNumber.Name = "textBoxRegisterNumber";
            this.textBoxRegisterNumber.Size = new System.Drawing.Size(100, 20);
            this.textBoxRegisterNumber.TabIndex = 1;
            this.textBoxRegisterNumber.Text = "DW123456";
            // 
            // textBoxSupply
            // 
            this.textBoxSupply.Location = new System.Drawing.Point(254, 24);
            this.textBoxSupply.Name = "textBoxSupply";
            this.textBoxSupply.Size = new System.Drawing.Size(100, 20);
            this.textBoxSupply.TabIndex = 2;
            this.textBoxSupply.Text = "Komputery";
            // 
            // textBoxAmount
            // 
            this.textBoxAmount.Location = new System.Drawing.Point(362, 24);
            this.textBoxAmount.Name = "textBoxAmount";
            this.textBoxAmount.Size = new System.Drawing.Size(117, 20);
            this.textBoxAmount.TabIndex = 3;
            this.textBoxAmount.Text = "10";
            // 
            // labelRegisterNumber
            // 
            this.labelRegisterNumber.AutoSize = true;
            this.labelRegisterNumber.Location = new System.Drawing.Point(145, 8);
            this.labelRegisterNumber.Name = "labelRegisterNumber";
            this.labelRegisterNumber.Size = new System.Drawing.Size(99, 13);
            this.labelRegisterNumber.TabIndex = 4;
            this.labelRegisterNumber.Text = "Numer rejestracyjny";
            // 
            // labelSupply
            // 
            this.labelSupply.AutoSize = true;
            this.labelSupply.Location = new System.Drawing.Point(255, 8);
            this.labelSupply.Name = "labelSupply";
            this.labelSupply.Size = new System.Drawing.Size(37, 13);
            this.labelSupply.TabIndex = 5;
            this.labelSupply.Text = "Towar";
            // 
            // labelAmount
            // 
            this.labelAmount.AutoSize = true;
            this.labelAmount.Location = new System.Drawing.Point(359, 8);
            this.labelAmount.Name = "labelAmount";
            this.labelAmount.Size = new System.Drawing.Size(29, 13);
            this.labelAmount.TabIndex = 6;
            this.labelAmount.Text = "Ilość";
            // 
            // dataGridViewArrivals
            // 
            this.dataGridViewArrivals.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewArrivals.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewArrivals.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewArrivals.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnRegisterNumber,
            this.ColumnSupply,
            this.ColumnAmount});
            this.dataGridViewArrivals.Location = new System.Drawing.Point(13, 50);
            this.dataGridViewArrivals.Name = "dataGridViewArrivals";
            this.dataGridViewArrivals.Size = new System.Drawing.Size(468, 221);
            this.dataGridViewArrivals.TabIndex = 7;
            // 
            // ColumnRegisterNumber
            // 
            this.ColumnRegisterNumber.HeaderText = "Numer rejestracyjny";
            this.ColumnRegisterNumber.Name = "ColumnRegisterNumber";
            // 
            // ColumnSupply
            // 
            this.ColumnSupply.HeaderText = "Towar";
            this.ColumnSupply.Name = "ColumnSupply";
            // 
            // ColumnAmount
            // 
            this.ColumnAmount.HeaderText = "Ilość";
            this.ColumnAmount.Name = "ColumnAmount";
            // 
            // buttonSaveToFile
            // 
            this.buttonSaveToFile.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonSaveToFile.Location = new System.Drawing.Point(262, 16);
            this.buttonSaveToFile.Name = "buttonSaveToFile";
            this.buttonSaveToFile.Size = new System.Drawing.Size(73, 36);
            this.buttonSaveToFile.TabIndex = 8;
            this.buttonSaveToFile.Text = "Zapisz";
            this.buttonSaveToFile.UseVisualStyleBackColor = true;
            this.buttonSaveToFile.Click += new System.EventHandler(this.buttonSaveToFile_Click);
            // 
            // buttonReadFromFile
            // 
            this.buttonReadFromFile.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonReadFromFile.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonReadFromFile.Location = new System.Drawing.Point(189, 16);
            this.buttonReadFromFile.Name = "buttonReadFromFile";
            this.buttonReadFromFile.Size = new System.Drawing.Size(73, 36);
            this.buttonReadFromFile.TabIndex = 9;
            this.buttonReadFromFile.Text = "Wczytaj";
            this.buttonReadFromFile.UseVisualStyleBackColor = true;
            this.buttonReadFromFile.Click += new System.EventHandler(this.buttonReadFromFile_Click);
            // 
            // groupBoxOptions
            // 
            this.groupBoxOptions.Controls.Add(this.textBoxFilter);
            this.groupBoxOptions.Controls.Add(this.labelFilter);
            this.groupBoxOptions.Controls.Add(this.buttonReadFromFile);
            this.groupBoxOptions.Controls.Add(this.buttonSaveToFile);
            this.groupBoxOptions.Controls.Add(this.buttonGenerateArrivals);
            this.groupBoxOptions.Controls.Add(this.buttonStatistics);
            this.groupBoxOptions.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBoxOptions.Location = new System.Drawing.Point(0, 277);
            this.groupBoxOptions.Name = "groupBoxOptions";
            this.groupBoxOptions.Size = new System.Drawing.Size(484, 55);
            this.groupBoxOptions.TabIndex = 10;
            this.groupBoxOptions.TabStop = false;
            this.groupBoxOptions.Text = "Opcje";
            // 
            // textBoxFilter
            // 
            this.textBoxFilter.Dock = System.Windows.Forms.DockStyle.Top;
            this.textBoxFilter.Location = new System.Drawing.Point(3, 29);
            this.textBoxFilter.Name = "textBoxFilter";
            this.textBoxFilter.Size = new System.Drawing.Size(186, 20);
            this.textBoxFilter.TabIndex = 11;
            this.textBoxFilter.TextChanged += new System.EventHandler(this.textBoxFilter_TextChanged);
            // 
            // labelFilter
            // 
            this.labelFilter.AutoSize = true;
            this.labelFilter.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelFilter.Location = new System.Drawing.Point(3, 16);
            this.labelFilter.Name = "labelFilter";
            this.labelFilter.Size = new System.Drawing.Size(39, 13);
            this.labelFilter.TabIndex = 10;
            this.labelFilter.Text = "Szukaj";
            // 
            // buttonGenerateArrivals
            // 
            this.buttonGenerateArrivals.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonGenerateArrivals.Location = new System.Drawing.Point(335, 16);
            this.buttonGenerateArrivals.Name = "buttonGenerateArrivals";
            this.buttonGenerateArrivals.Size = new System.Drawing.Size(73, 36);
            this.buttonGenerateArrivals.TabIndex = 12;
            this.buttonGenerateArrivals.Text = "Generator";
            this.buttonGenerateArrivals.UseVisualStyleBackColor = true;
            this.buttonGenerateArrivals.Click += new System.EventHandler(this.buttonGenerateArrivals_Click);
            // 
            // buttonStatistics
            // 
            this.buttonStatistics.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonStatistics.Location = new System.Drawing.Point(408, 16);
            this.buttonStatistics.Name = "buttonStatistics";
            this.buttonStatistics.Size = new System.Drawing.Size(73, 36);
            this.buttonStatistics.TabIndex = 13;
            this.buttonStatistics.Text = "Statystyki";
            this.buttonStatistics.UseVisualStyleBackColor = true;
            this.buttonStatistics.Click += new System.EventHandler(this.buttonStatistics_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 332);
            this.Controls.Add(this.groupBoxOptions);
            this.Controls.Add(this.dataGridViewArrivals);
            this.Controls.Add(this.labelAmount);
            this.Controls.Add(this.labelSupply);
            this.Controls.Add(this.labelRegisterNumber);
            this.Controls.Add(this.textBoxAmount);
            this.Controls.Add(this.textBoxSupply);
            this.Controls.Add(this.textBoxRegisterNumber);
            this.Controls.Add(this.buttonNew);
            this.MinimumSize = new System.Drawing.Size(500, 370);
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Rejestr przyjazdów";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewArrivals)).EndInit();
            this.groupBoxOptions.ResumeLayout(false);
            this.groupBoxOptions.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonNew;
        private System.Windows.Forms.TextBox textBoxRegisterNumber;
        private System.Windows.Forms.TextBox textBoxSupply;
        private System.Windows.Forms.TextBox textBoxAmount;
        private System.Windows.Forms.Label labelRegisterNumber;
        private System.Windows.Forms.Label labelSupply;
        private System.Windows.Forms.Label labelAmount;
        private System.Windows.Forms.DataGridView dataGridViewArrivals;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnRegisterNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnSupply;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnAmount;
        private System.Windows.Forms.Button buttonSaveToFile;
        private System.Windows.Forms.Button buttonReadFromFile;
        private System.Windows.Forms.GroupBox groupBoxOptions;
        private System.Windows.Forms.TextBox textBoxFilter;
        private System.Windows.Forms.Label labelFilter;
        private System.Windows.Forms.Button buttonGenerateArrivals;
        private System.Windows.Forms.Button buttonStatistics;
    }
}

