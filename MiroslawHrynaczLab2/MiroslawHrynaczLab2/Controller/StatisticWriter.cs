﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace MiroslawHrynaczLab2.Controller
{
    public class StatisticWriter
    {
        #region Private Fields
        private readonly DataGridView dataGridView;
        private readonly RichTextBox richTextBox;
        private readonly Thread thread;
        #endregion

        /// <summary>
        /// Konstruktor klasy
        /// </summary>
        /// <param name="dataGridView">Źródło danych do wyświetlenia</param>
        /// <param name="richTextBox">Docelowe miejsce wyświelania danych</param>
        public StatisticWriter(DataGridView dataGridView, RichTextBox richTextBox)
        {
            this.dataGridView = dataGridView;
            this.richTextBox = richTextBox;
            thread = new Thread(UpdateStatisticsContinously);
        }

        /// <summary>
        /// Funkcja rozpoczyna wyświetlanie statystyk
        /// </summary>
        public void StartWriting()
        {
            thread.Start();
        }

        /// <summary>
        /// Funkcja zatrzymuje aktualizowanie statystyk
        /// </summary>
        public void StopWriting()
        {
            thread.Abort();
            while (thread.ThreadState != ThreadState.Aborted) { }
        }

        /// <summary>
        /// Funkcja z nieskończoną pętlą co sekundę odświeża wyświetlane dane
        /// </summary>
        private void UpdateStatisticsContinously()
        {
            while (true)
            {
                try
                {
                    UpdateStatistics();
                    Thread.Sleep(1000);
                }
                catch (InvalidOperationException)
                {
                    // Przerwano operację
                    break;
                }
            }
        }

        /// <summary>
        /// Generowanie komunikatu do wyświetlenia
        /// </summary>
        private void UpdateStatistics()
        {
            var sumOfEachSupplies = ReadDataFromGrid();
            var totalSum = sumOfEachSupplies.Sum(supply => supply.Value);

            var message = String.Format("Całkowita ilość towaru = {0}\n\n", totalSum);
            foreach (var supply in sumOfEachSupplies)
            {
                message += String.Format("Towar :{0}, ilość = {1}\n", supply.Key, supply.Value);
            }
            richTextBox.Invoke(new Action(() => { richTextBox.Text = message; }));
        }

        /// <summary>
        /// Funkcja odczytuje dane z źródła i kategoryzuje wg rodzaju towaru i jego ilości
        /// </summary>
        /// <returns>Słownik o kluczach jako rodzaj towaru, z wartościami równymi ilości towaru</returns>
        private Dictionary<string, int> ReadDataFromGrid()
        {
            var sumOfEachSupplies = new Dictionary<string, int>();
            for (int row = 0; row < dataGridView.RowCount - 1; row++)
            {
                if (sumOfEachSupplies.ContainsKey(dataGridView[1, row].Value.ToString()))
                {
                    sumOfEachSupplies[dataGridView[1, row].Value.ToString()] += int.Parse(dataGridView[2, row].Value.ToString());
                }
                else
                {
                    sumOfEachSupplies.Add(dataGridView[1, row].Value.ToString(),
                        int.Parse(dataGridView[2, row].Value.ToString()));
                }
            }
            return sumOfEachSupplies;
        }
    }
}