using System;
using System.Threading;
using System.Windows.Forms;

namespace MiroslawHrynaczLab2.Controller
{
    /// <summary>
    /// Klasa generuj�ca dane o przyje�dzaj�cych pojazdach oraz ich towar�w
    /// </summary>
    public class Generator
    {
        #region Private Fields
        private readonly object[] supplies = {"Komputer", "TV", "Radio", "Smartfon", "Laptop"};
        private readonly object[] licenseNumbers = { "DW", "WW", "PO", "ZS", "KR" };
        readonly Random randomNumbers = new Random();
        private readonly DataGridView dataGridView;
        private readonly TrackBar trackBar;
        private Thread thread;
        private int isRunning;
        #endregion

        /// <summary>
        /// Konstruktor klasy generator
        /// </summary>
        /// <param name="dataGridView">Obiekt do kt�rego generowane s� dane</param>
        /// <param name="trackBar">�r�d�o parametru algorytmu - pr�dko�� generowania</param>
        public Generator(DataGridView dataGridView, TrackBar trackBar)
        {
            this.dataGridView = dataGridView;
            this.trackBar = trackBar;
            thread = new Thread(GeneratingThreadBody);
        }

        /// <summary>
        /// funkcja generuj�ca jedn� z dost�pnych permutacji tablicy rejestracyjnej,
        /// dost�pnego towaru oraz jego ilo�ci
        /// </summary>
        /// <returns>Tablica zawieraj�ca {tablice rejestracyjn�, nazw� towaru, ilo�� towaru</returns>
        private object[] generate()
        {
            var suppliesIndex = randomNumbers.Next(0, supplies.Length);
            var licensePrefix = randomNumbers.Next(0, licenseNumbers.Length);
            var licenseNumber = licenseNumbers[licensePrefix] + randomNumbers.Next(1000, 9999).ToString();
            var amountOfSupply = randomNumbers.Next(0, 1000);

            return new[] {licenseNumber, supplies[suppliesIndex], amountOfSupply};
        }

        /// <summary>
        /// Funkcja generuj�ca niesko�czenie wiele losowych danych
        /// </summary>
        private void GeneratingThreadBody()
        {
            while (isRunning != 0)
            {
                dataGridView.Invoke(new Action(() => dataGridView.Rows.Add(generate())));
                int timeToSleep = 0;
                trackBar.Invoke(new Action(() => timeToSleep = trackBar.Value));
                Thread.Sleep(20000 / timeToSleep);
            }
        }


        /// <summary>
        /// Rozpoczyna generowanie danych
        /// </summary>
        public void StartGenerating()
        {
            if (thread.ThreadState != ThreadState.Running)
            {
                thread = new Thread(GeneratingThreadBody);
                Interlocked.Increment(ref isRunning);
                thread.Start();
            }
        }
        
        /// <summary>
        /// Zatrzymuje generowanie danych
        /// </summary>
        public void StopGenerating()
        {
            if (isRunning != 0)
            {
                Interlocked.Decrement(ref isRunning);
                thread.Abort();
                while (thread.ThreadState != ThreadState.Aborted) { }
            }
        }
    }
}