﻿using System;
using System.IO;
using System.Windows.Forms;

namespace MiroslawHrynaczLab2.Controller
{
    public static class FormMainController
    {
        /// <summary>
        /// Funkcja zapisuje podany objekt DataGridView do wskazanego pliku
        /// </summary>
        /// <param name="sourceDataGridView">Źródłowa tabela</param>
        /// <param name="fileName">Nazwa pliku docelowego</param>
        public static void SaveToFile(DataGridView sourceDataGridView, string fileName = "test.txt")
        {
            var writer = new StreamWriter(fileName);

            for (var i = 0; i < sourceDataGridView.RowCount - 1; i++)
            {
                for (var j = 0; j < sourceDataGridView.ColumnCount; j++)
                {
                    writer.Write(sourceDataGridView[j, i].Value);
                    if (j < sourceDataGridView.ColumnCount - 1)
                    {
                        writer.Write(";");
                    }
                }
                writer.Write(Environment.NewLine);
            }
            writer.Close(); 
        }

        /// <summary>
        /// Funkcja odczytuje dane z pliku i zapisuje w podanym DataGridView.
        /// Format pliku : [column[;column]*\n]+
        /// </summary>
        /// <param name="dataGridView"></param>
        /// <param name="fileName">Nazwa pliku źródłowego</param>
        public static void LoadFromFile(DataGridView dataGridView, string fileName = "test.txt")
        {
            dataGridView.Rows.Clear();
            var reader = new StreamReader(fileName);

            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                object[] columns = line.Split(';');
                dataGridView.Rows.Add(columns);
            }
            reader.Close();
        }

        /// <summary>
        /// Funkcja filtruje wiersze ze względu na to czy zawierają podany ciąg znaków
        /// </summary>
        /// <param name="dataGridViewArrivals">DataGridView, który jest filtrowany</param>
        /// <param name="filter">Dowolny ciąg znaków jaki powinien zawierać wiersz</param>
        public static void FilterRows(DataGridView dataGridViewArrivals, string filter)
        {
            for (int index = 0; index < dataGridViewArrivals.RowCount - 1; index++)
            {
                var row = dataGridViewArrivals.Rows[index];
                var rowText = "";
                for (var i = 0; i < dataGridViewArrivals.ColumnCount; i++) // łączenie danych z kolumn w jeden napis
                {
                    rowText += dataGridViewArrivals[i, index].Value + " ";
                }
                row.Visible = rowText.Contains(filter); // ukrywanie wierszy jeżeli nie posiadają wspólnego podciągu
            }
        }
    }
}
