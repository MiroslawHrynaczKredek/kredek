﻿using System;
using System.Windows.Forms;

namespace MiroslawHrynaczLab4GraB
{
    public partial class FormRegister : Form
    {
        private string prevPlayerName = "";

        public FormRegister()
        {
            InitializeComponent();
            buttonRegister.Click += buttonRegister_Click;
        }

        private FormRegister(string firstPlayerName)
        {
            InitializeComponent();
            prevPlayerName = firstPlayerName;
            buttonRegister.Click += buttonRegister_Click2;
        }

        private void buttonRegister_Click(object sender, EventArgs e)
        {
            var secoundPlayer = new FormRegister(textBoxPlayerName.Text);
            Visible = false; 
            secoundPlayer.Show();
            buttonRegister.Click -= buttonRegister_Click;
        }

        private void buttonRegister_Click2(object sender, EventArgs e)
        {
            var board = new GameBoard(prevPlayerName, textBoxPlayerName.Text);
            Visible = false;
            board.Show();
            buttonRegister.Click -= buttonRegister_Click2;
        }
    }
}
