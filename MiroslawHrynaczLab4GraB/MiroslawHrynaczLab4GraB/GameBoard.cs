﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace MiroslawHrynaczLab4GraB
{
    enum ActualPlayer
    {
        PlayerA = -1,
        PlayerB = 1
    }

    public partial class GameBoard : Form
    {
        private int tossInGame = 20;
        private ActualPlayer actuaPlayer;

        DataClassesGameDataContext dataContext = new DataClassesGameDataContext();
        private int playerA;
        private int playerB;
        private int matchID;
        private int[] listOfPositions;

        private Game gameA;
        private Game gameB;

        public GameBoard(string firstPlayerName, string secoundPlayerName)
        {
            InitializeComponent();
            var firstPlayer = new Player { Name = firstPlayerName };
            var secoundPlayer = new Player { Name = secoundPlayerName };
            dataContext.Players.InsertOnSubmit(firstPlayer);
            dataContext.Players.InsertOnSubmit(secoundPlayer);
            dataContext.SubmitChanges();

            listOfPositions = new int[20];
            for (var i = 0; i < listOfPositions.Length; i++)
            {
                listOfPositions[i] = 0;
            }
            labelPlayerAName.Text = firstPlayerName;
            labelPlayerBName.Text = secoundPlayerName;
            labelPositionA.Text = "-1";
            labelPositionB.Text = "-1";
            labelPlayerAScore.Text = "Wynik : 0";
            labelPlayerBScore.Text = "Wynik : 0";
            textBoxGeneratedNumber.Text = "0";

            var match = new Match {Date = DateTime.Today, WinnerPlayerID = 0};
            dataContext.Matches.InsertOnSubmit(match);
            dataContext.SubmitChanges();

            actuaPlayer = (ActualPlayer)new Random().Next(0, 1);

            playerA = (from x in dataContext.Players where x.Name.StartsWith(firstPlayerName) select x.ID).First();
            playerB = (from x in dataContext.Players where x.Name.StartsWith(secoundPlayerName) select x.ID).First();

            matchID = (from game in dataContext.Matches orderby game.ID select game.ID).First();
            gameA = new Game {MatchID = matchID, PlayerID = playerA, Position = -1, Score = 0};
            gameB = new Game {MatchID = matchID, PlayerID = playerB, Position = -1, Score = 0};
            dataContext.Games.InsertOnSubmit(gameA);
            dataContext.Games.InsertOnSubmit(gameB);
            dataContext.SubmitChanges();
        }

        private void buttonRandomize_Click(object sender, EventArgs e)
        {
            var randomGenerator = new Random();
            textBoxGeneratedNumber.Text = randomGenerator.Next(0, 6).ToString();
        }

        private void buttonGoForward_Click(object sender, EventArgs e)
        {
            buttonRandomize_Click(null, null);
            if (--tossInGame == 0)
            {
                buttonGoForward.Enabled = false;
            }

            if (actuaPlayer == ActualPlayer.PlayerA)
            {
                gameA.Position += int.Parse(textBoxGeneratedNumber.Text);
                gameA.Score += int.Parse(textBoxGeneratedNumber.Text) * int.Parse(textBoxGeneratedNumber.Text);
                labelPositionA.Text = gameA.Position.ToString();
                labelPlayerAScore.Text = gameA.Position.ToString();
                actuaPlayer = ActualPlayer.PlayerB;
            }
            else
            {
                gameB.Position += int.Parse(textBoxGeneratedNumber.Text);
                gameB.Score += int.Parse(textBoxGeneratedNumber.Text) * int.Parse(textBoxGeneratedNumber.Text);
                labelPositionB.Text = gameB.Position.ToString();
                labelPlayerBScore.Text = gameB.Score.ToString();
                actuaPlayer = ActualPlayer.PlayerA;
            }
            dataContext.SubmitChanges();
            
        }
    }
}
