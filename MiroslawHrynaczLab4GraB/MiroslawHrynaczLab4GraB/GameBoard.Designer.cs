﻿namespace MiroslawHrynaczLab4GraB
{
    partial class GameBoard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonRandomize = new System.Windows.Forms.Button();
            this.textBoxGeneratedNumber = new System.Windows.Forms.TextBox();
            this.labelPlayerAName = new System.Windows.Forms.Label();
            this.labelPlayerAScore = new System.Windows.Forms.Label();
            this.labelPlayerBName = new System.Windows.Forms.Label();
            this.labelPlayerBScore = new System.Windows.Forms.Label();
            this.buttonGoForward = new System.Windows.Forms.Button();
            this.labelPositionA = new System.Windows.Forms.Label();
            this.labelPositionB = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonRandomize
            // 
            this.buttonRandomize.Enabled = false;
            this.buttonRandomize.Location = new System.Drawing.Point(16, 136);
            this.buttonRandomize.Name = "buttonRandomize";
            this.buttonRandomize.Size = new System.Drawing.Size(75, 23);
            this.buttonRandomize.TabIndex = 0;
            this.buttonRandomize.Text = "losuj!";
            this.buttonRandomize.UseVisualStyleBackColor = true;
            this.buttonRandomize.Click += new System.EventHandler(this.buttonRandomize_Click);
            // 
            // textBoxGeneratedNumber
            // 
            this.textBoxGeneratedNumber.Enabled = false;
            this.textBoxGeneratedNumber.Location = new System.Drawing.Point(64, 110);
            this.textBoxGeneratedNumber.Name = "textBoxGeneratedNumber";
            this.textBoxGeneratedNumber.ShortcutsEnabled = false;
            this.textBoxGeneratedNumber.Size = new System.Drawing.Size(75, 20);
            this.textBoxGeneratedNumber.TabIndex = 1;
            // 
            // labelPlayerAName
            // 
            this.labelPlayerAName.AutoSize = true;
            this.labelPlayerAName.Location = new System.Drawing.Point(13, 9);
            this.labelPlayerAName.Name = "labelPlayerAName";
            this.labelPlayerAName.Size = new System.Drawing.Size(14, 13);
            this.labelPlayerAName.TabIndex = 2;
            this.labelPlayerAName.Text = "A";
            // 
            // labelPlayerAScore
            // 
            this.labelPlayerAScore.AutoSize = true;
            this.labelPlayerAScore.Location = new System.Drawing.Point(13, 60);
            this.labelPlayerAScore.Name = "labelPlayerAScore";
            this.labelPlayerAScore.Size = new System.Drawing.Size(13, 13);
            this.labelPlayerAScore.TabIndex = 3;
            this.labelPlayerAScore.Text = "0";
            // 
            // labelPlayerBName
            // 
            this.labelPlayerBName.AutoSize = true;
            this.labelPlayerBName.Location = new System.Drawing.Point(170, 9);
            this.labelPlayerBName.Name = "labelPlayerBName";
            this.labelPlayerBName.Size = new System.Drawing.Size(14, 13);
            this.labelPlayerBName.TabIndex = 4;
            this.labelPlayerBName.Text = "B";
            // 
            // labelPlayerBScore
            // 
            this.labelPlayerBScore.AutoSize = true;
            this.labelPlayerBScore.Location = new System.Drawing.Point(171, 60);
            this.labelPlayerBScore.Name = "labelPlayerBScore";
            this.labelPlayerBScore.Size = new System.Drawing.Size(13, 13);
            this.labelPlayerBScore.TabIndex = 5;
            this.labelPlayerBScore.Text = "0";
            // 
            // buttonGoForward
            // 
            this.buttonGoForward.Location = new System.Drawing.Point(98, 136);
            this.buttonGoForward.Name = "buttonGoForward";
            this.buttonGoForward.Size = new System.Drawing.Size(75, 23);
            this.buttonGoForward.TabIndex = 6;
            this.buttonGoForward.Text = "Ruch!";
            this.buttonGoForward.UseVisualStyleBackColor = true;
            this.buttonGoForward.Click += new System.EventHandler(this.buttonGoForward_Click);
            // 
            // labelPositionA
            // 
            this.labelPositionA.AutoSize = true;
            this.labelPositionA.Location = new System.Drawing.Point(13, 35);
            this.labelPositionA.Name = "labelPositionA";
            this.labelPositionA.Size = new System.Drawing.Size(35, 13);
            this.labelPositionA.TabIndex = 7;
            this.labelPositionA.Text = "label1";
            // 
            // labelPositionB
            // 
            this.labelPositionB.AutoSize = true;
            this.labelPositionB.Location = new System.Drawing.Point(149, 35);
            this.labelPositionB.Name = "labelPositionB";
            this.labelPositionB.Size = new System.Drawing.Size(35, 13);
            this.labelPositionB.TabIndex = 8;
            this.labelPositionB.Text = "label2";
            // 
            // GameBoard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(218, 162);
            this.Controls.Add(this.labelPositionB);
            this.Controls.Add(this.labelPositionA);
            this.Controls.Add(this.buttonGoForward);
            this.Controls.Add(this.labelPlayerBScore);
            this.Controls.Add(this.labelPlayerBName);
            this.Controls.Add(this.labelPlayerAScore);
            this.Controls.Add(this.labelPlayerAName);
            this.Controls.Add(this.textBoxGeneratedNumber);
            this.Controls.Add(this.buttonRandomize);
            this.Name = "GameBoard";
            this.Text = "GameBoard";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonRandomize;
        private System.Windows.Forms.TextBox textBoxGeneratedNumber;
        private System.Windows.Forms.Label labelPlayerAName;
        private System.Windows.Forms.Label labelPlayerAScore;
        private System.Windows.Forms.Label labelPlayerBName;
        private System.Windows.Forms.Label labelPlayerBScore;
        private System.Windows.Forms.Button buttonGoForward;
        private System.Windows.Forms.Label labelPositionA;
        private System.Windows.Forms.Label labelPositionB;
    }
}