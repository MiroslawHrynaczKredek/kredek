﻿CREATE TABLE [dbo].[Table]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Name] NVARCHAR(50) NOT NULL, 
    [Surname] NVARCHAR(100) NOT NULL, 
    [DateStart] DATE NULL, 
    [Hostel] BIT NULL, 
    [IsStudent] BIT NULL, 
    [Average] FLOAT NULL
)
