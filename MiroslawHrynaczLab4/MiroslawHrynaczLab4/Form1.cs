﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiroslawHrynaczLab4
{
    public partial class Form1 : Form
    {
        DataClassesLinqDataContext dc = new DataClassesLinqDataContext();

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var query = from c in dc.Products select c;
            dataGridViewDataBase.DataSource = query;
        }
    }
}
